﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace clock
{
    class Nums
    {
        short[][] zero = {
            new short[]{ 1, 1, 1, 1 },
            new short[]{ 1, 0, 0, 1 },
            new short[]{ 1, 0, 0, 1 },
            new short[]{ 1, 0, 0, 1 },
            new short[]{ 1, 0, 0, 1 },
            new short[]{ 1, 0, 0, 1 },
            new short[]{ 1, 1, 1, 1 },
        };
        short[][] one = {
            new short[]{ 0, 0, 0, 1 },
            new short[]{ 0, 0, 0, 1 },
            new short[]{ 0, 0, 0, 1 },
            new short[]{ 0, 0, 0, 1 },
            new short[]{ 0, 0, 0, 1 },
            new short[]{ 0, 0, 0, 1 },
            new short[]{ 0, 0, 0, 1 },
        };
        short[][] two = {
            new short[]{ 1, 1, 1, 1 },
            new short[]{ 0, 0, 0, 1 },
            new short[]{ 0, 0, 0, 1 },
            new short[]{ 1, 1, 1, 1 },
            new short[]{ 1, 0, 0, 0 },
            new short[]{ 1, 0, 0, 0 },
            new short[]{ 1, 1, 1, 1 },
        };
        short[][] three = {
            new short[]{ 1, 1, 1, 1 },
            new short[]{ 0, 0, 0, 1 },
            new short[]{ 0, 0, 0, 1 },
            new short[]{ 1, 1, 1, 1 },
            new short[]{ 0, 0, 0, 1 },
            new short[]{ 0, 0, 0, 1 },
            new short[]{ 1, 1, 1, 1 },
        };
        short[][] four = {
            new short[]{ 1, 0, 0, 1 },
            new short[]{ 1, 0, 0, 1 },
            new short[]{ 1, 0, 0, 1 },
            new short[]{ 1, 1, 1, 1 },
            new short[]{ 0, 0, 0, 1 },
            new short[]{ 0, 0, 0, 1 },
            new short[]{ 0, 0, 0, 1 },
        };
        short[][] five = {
            new short[]{ 1, 1, 1, 1 },
            new short[]{ 1, 0, 0, 0 },
            new short[]{ 1, 0, 0, 0 },
            new short[]{ 1, 1, 1, 1 },
            new short[]{ 0, 0, 0, 1 },
            new short[]{ 0, 0, 0, 1 },
            new short[]{ 1, 1, 1, 1 },
        };
        short[][] six = {
            new short[]{ 1, 1, 1, 1 },
            new short[]{ 1, 0, 0, 0 },
            new short[]{ 1, 0, 0, 0 },
            new short[]{ 1, 1, 1, 1 },
            new short[]{ 1, 0, 0, 1 },
            new short[]{ 1, 0, 0, 1 },
            new short[]{ 1, 1, 1, 1 },
        };
        short[][] seven = {
            new short[]{ 1, 1, 1, 1 },
            new short[]{ 0, 0, 0, 1 },
            new short[]{ 0, 0, 0, 1 },
            new short[]{ 0, 0, 0, 1 },
            new short[]{ 0, 0, 0, 1 },
            new short[]{ 0, 0, 0, 1 },
            new short[]{ 0, 0, 0, 1 },
        };
        short[][] eight = {
            new short[]{ 1, 1, 1, 1 },
            new short[]{ 1, 0, 0, 1 },
            new short[]{ 1, 0, 0, 1 },
            new short[]{ 1, 1, 1, 1 },
            new short[]{ 1, 0, 0, 1 },
            new short[]{ 1, 0, 0, 1 },
            new short[]{ 1, 1, 1, 1 },
        };
        short[][] nine = {
            new short[]{ 1, 1, 1, 1 },
            new short[]{ 1, 0, 0, 1 },
            new short[]{ 1, 0, 0, 1 },
            new short[]{ 1, 1, 1, 1 },
            new short[]{ 0, 0, 0, 1 },
            new short[]{ 0, 0, 0, 1 },
            new short[]{ 1, 1, 1, 1 },
        };
        short[][] colon = {
            new short[]{ 0, 0, 0, 0 },
            new short[]{ 0, 1, 1, 0 },
            new short[]{ 0, 1, 1, 0 },
            new short[]{ 0, 0, 0, 0 },
            new short[]{ 0, 1, 1, 0 },
            new short[]{ 0, 1, 1, 0 },
            new short[]{ 0, 0, 0, 0 },
        };

        Dictionary<char, short[][]> nums = new Dictionary<char, short[][]>(11);

        public Nums()
        {
            nums.Add('0', zero);
            nums.Add('1', one);
            nums.Add('2', two);
            nums.Add('3', three);
            nums.Add('4', four);
            nums.Add('5', five);
            nums.Add('6', six);
            nums.Add('7', seven);
            nums.Add('8', eight);
            nums.Add('9', nine);
            nums.Add(':', colon);
        }

        public short[][] getNum(char num)
        {
            return nums[num];
        }
    }
}
