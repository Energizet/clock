﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace clock
{
    class Program
    {
        static void Main(string[] args)
        {
            bool negative = false;
            if (args.Length == 0)
            {
                negative = false; // true = black on white; false = white on black 
            }
            else if (args[0] == "-n" || args[0] == "--negative")
            {
                negative = true;
            }

            int cursorLeft = Console.CursorLeft;
            int cursorTop = Console.CursorTop;

            Nums nums = new Nums();
            while (true)
            {
                char[] time = DateTime.Now.GetDateTimeFormats('T')[0].ToCharArray();
                short[][][] numChars = new short[time.Length][][];
                for (int i = 0; i < time.Length; i++)
                {
                    numChars[i] = nums.getNum(time[i]);
                }

                short[][] numLines = new short[9][];
                int size = time.Length * 4 + (time.Length + 1);
                for (int i = 0; i < numLines.Length; i++)
                {
                    numLines[i] = new short[size];
                    for (int j = 0; j < size; j++)
                    {
                        numLines[i][j] = 0;
                    }
                }

                for (int i = 0; i < numChars[0].Length; i++)
                {
                    for (int j = 0; j < numChars.Length; j++)
                    {
                        for (int k = 0; k < numChars[j][i].Length; k++)
                        {
                            numLines[i + 1][j * 4 + k + (j + 1)] = numChars[j][i][k];
                        }
                    }
                }

                for (int i = 0; i < numLines.Length; i++)
                {
                    for (int j = 0; j < numLines[i].Length; j++)
                    {
                        if (numLines[i][j] == 1)
                        {
                            if (negative)
                            {
                                Console.ResetColor();
                            }
                            else
                            {
                                Console.BackgroundColor = ConsoleColor.White;
                            }
                            Console.Write(' ');
                        }
                        else
                        {
                            if (negative)
                            {
                                Console.BackgroundColor = ConsoleColor.White;
                            }
                            else
                            {
                                Console.ResetColor();
                            }
                            Console.Write(' ');
                        }
                    }
                    Console.WriteLine();
                }
                Console.ResetColor();

                Thread.Sleep(240);
                Console.CursorLeft = cursorLeft;
                Console.CursorTop = cursorTop;
            }
        }
    }
}
